using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationPlay : MonoBehaviour
{

    GetClickName random;

    public GameObject HumanModel;
    
    public Animator anim;
    [HideInInspector]
    public AudioSource audioSource;
    int randomnum;
    // Start is called before the first frame update
    void Start()
    {
        HumanModel.SetActive(false);
        random = GetComponent<GetClickName>();
        anim = GetComponent<Animator>();
       audioSource = GetComponentInParent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        

       
    }

    public void SpawnHuman()
    { 
        HumanModel.SetActive(true);
       this.transform.localPosition = new Vector3(0, 8, 0);

        

    
    }


    public void RunAnimation( int randomInt)
    {
       
       
       

        
        anim.SetInteger("RandomNum", randomInt);

    }

}
