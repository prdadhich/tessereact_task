using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GetClickName : MonoBehaviour
{
    public Camera cam;
    LoadMusic loadMusic;
    SpawnPrefab spawnPrefab;
    Animator animPlay;
    
    public GameObject animplaygameobject;
    public string SelectedObject;


    public int indextoPass = 0;
    private string firstSelected = null;
    public int randomInt = 0;
    private string previousName;
    private string currentName;

    // Start is called before the first frame update
    void Start()
    {
        loadMusic = GetComponent<LoadMusic>();
        spawnPrefab = GetComponent<SpawnPrefab>();
        animPlay = animplaygameobject.GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        


        if(Input.GetMouseButtonDown(0))
        {

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray,out RaycastHit hitInfo))
            {
                /*if(loadMusic.playList.Contains(hitInfo.collider.gameObject.name))
                {

                    Debug.Log(hitInfo.collider.gameObject.name);
                }*/
                SelectedObject = hitInfo.collider.gameObject.name;
                if(loadMusic.playList.Contains(SelectedObject))
                {
                    loadMusic.UpdateSongs(SelectedObject);
                }
                if (loadMusic.songList != null && loadMusic.songList.Contains(SelectedObject))
                {
                    
                    for (int i  =0; i< loadMusic.songList.Length;i++)
                    { 
                        if(loadMusic.songList[i] == SelectedObject)
                        {
                            indextoPass = i;
                        }
                    
                    }
                    PlaySong(SelectedObject, indextoPass);


                }
            }
            
        }
    }
    public void  NextSong()
    {
        indextoPass +=1;

        PlaySong("null", indextoPass);


    }

    void PlaySong(string selectedObject, int index)
    {


         loadMusic.WhichSong(spawnPrefab.obj[index].name);


        SelectedObject = spawnPrefab.obj[index].name;

        currentName = SelectedObject;

            if (firstSelected == null)
            {

            }

            if (firstSelected != null)
            {


                spawnPrefab.ResetPos(previousName);

            }



            firstSelected = SelectedObject;




            spawnPrefab.obj[index].gameObject.transform.position = new Vector3(0, 1, -20);
            spawnPrefab.obj[index].gameObject.GetComponent<GetHoverObject>().RotateHorizontal();

            previousName = currentName;

            int randomnum;
            animplaygameobject.GetComponent<AnimationPlay>().SpawnHuman();
            animplaygameobject.GetComponent<AnimationPlay>().RunAnimation(randomnum = Random.Range(0, 3));





        }
 }
  



