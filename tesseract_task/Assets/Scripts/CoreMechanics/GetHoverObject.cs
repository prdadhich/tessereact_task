using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetHoverObject : MonoBehaviour
{
    public GameObject rotateDisc;
    public bool rotate = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(rotate)
        {
            rotateDisc.transform.Rotate(0, 0, 1, Space.Self);
        }
    }

    private void OnMouseEnter()
    {
        this.gameObject.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        rotate = true;
    }

    private void OnMouseExit()
    {
        gameObject.transform.localScale = new Vector3(1,1,1);
        rotate = false;
    }
    public void RotateHorizontal()
    {
        rotateDisc.transform.Rotate(70, 0, 0, Space.Self);

    }
    public void ResetRotate()
    {

        rotateDisc.transform.rotation = Quaternion.identity;
    }

}
