using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    GetClickName getClickName;

    private AudioSource m_AudioSource;
    public GameObject PlayPause;
    public GameObject sliderObject;
    [HideInInspector]
    public Button PlayPauseButton;

    public Sprite Play;
    public Sprite Pause;
    public Slider slider;
    private bool pause = false;
    public float audioLength;
    // Start is called before the first frame update
    void Start()
    {
        getClickName = GetComponent<GetClickName>();
        m_AudioSource = GetComponent<AudioSource>();
        PlayPauseButton = GetComponent<Button>();   
        PlayPause.SetActive(false);
        sliderObject.SetActive(false);

        PlayPause.GetComponent<Image>().enabled = false;
        slider.onValueChanged.AddListener(OnSliderValueChange);
    }

    private void OnDisable() => slider.onValueChanged.RemoveAllListeners();
    private void OnSliderValueChange(float f) => m_AudioSource.time =  f*audioLength;
    // Update is called once per frame
    void Update()
    {
        if(m_AudioSource.isPlaying)
        {
            PlayPause.SetActive(false);
            sliderObject.SetActive(true);
            PlayPause.GetComponent<Image>().enabled = true;

            PlayPause.GetComponent<Image>().sprite =  Pause;

            slider.value = Mathf.Clamp(m_AudioSource.time / audioLength, 0, 1);
          
            if(m_AudioSource.time == audioLength)
            {
                getClickName.NextSong();
            }

        }
        
    }
   
    public void  TogglePlayPause()
    {
        pause = !pause;

        if (pause)
        {
            m_AudioSource.Pause();
            PlayPause.GetComponent<Image>().sprite = Play;

        }

        if(!pause)
        {
            m_AudioSource.UnPause();
            PlayPause.GetComponent<Image>().sprite = Pause;
        }


    }
}
