using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Networking;
using System.Linq;

public class LoadMusic : MonoBehaviour

   
{

    LoadJson musicList;
    SpawnPrefab spawnPrefab;
    GetClickName ClickedObject;
    AudioManager audioManager;

    private AudioSource musicSource;
    private readonly string musicPath = "file:///D:/TessMusicPlayer/";
    private string finalSongPath = null;
    private bool playListUpdated = false;
    private AudioClip audioClip;

    [HideInInspector]
    public string MusicToPlay = "Bekhayali.wav";
    [HideInInspector]
    public List<string> playList = new List<string>();
    [HideInInspector]
    public string[] songList;
    [HideInInspector]
    public int PlaylistNumber = 0;
    [HideInInspector]
    public int SonglistNumber = 0;


  
    // Start is called before the first frame update
    void Start()
    {
        musicList = GetComponent<LoadJson>();
        spawnPrefab = GetComponent<SpawnPrefab>();
        ClickedObject=GetComponent<GetClickName>();
        audioManager   = GetComponent<AudioManager>();  
        musicSource = GetComponent<AudioSource>();
        if (musicList != null && playListUpdated == false)
        {

            PlaylistUpdate();

        }
        
        
        //audioClips[0] = musicList
    }

    // Update is called once per frame
    private void Update()
    {
       
        
    }



    private void PlaylistUpdate()
    {
        foreach (var item in musicList.musicData.music)
        {

            playList.Add(item.playlistname);
           

        }
        playListUpdated = true;
        PlaylistNumber = playList.Count;
        
        

        spawnPrefab.Spawn(PlaylistNumber);
        if (spawnPrefab.obj != null)
        {
            for(int i=0; i< PlaylistNumber;i++)
            {
                spawnPrefab.obj[i].gameObject.name = playList[i];

            }
        }
        
        
    }


    IEnumerator PlayMusic()

    {
       // string clip1 = musicPath + musicList.musicData.music[0].name[0]+".mp3";
        using (UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(finalSongPath, AudioType.WAV))
        {
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError)
            {
                Debug.Log(www.error);

            }
            else
            {
                 
                audioClip = DownloadHandlerAudioClip.GetContent(www);
                
                musicSource.clip = audioClip;
                audioManager.audioLength = audioClip.length;
               
                musicSource.Play();

            }
        }

    }

    public void UpdateSongs(string Clicked)
    {

        int index = -1;

        index = playList.FindIndex(a => a.Contains(Clicked));
        
        if(index >=0)
        songList = musicList.musicData.music[index].name;
        
        for(int i =0; i< songList.Length;i++)
        {
            Debug.Log(songList[i]);
        }


        if (playList.Contains(Clicked))
        {
            DestroyObjects();
        }
        SonglistNumber = songList.Length;
        spawnPrefab.obj.Clear();
        spawnPrefab.Spawn(SonglistNumber);
        
        if (spawnPrefab.obj != null)
        {
            for (int i = 0; i < SonglistNumber; i++)
            {
                spawnPrefab.obj[i].gameObject.name = songList[i];
                
            }
            
        }

    }


    void DestroyObjects()
    { 
        foreach( var obj in playList)
        {
            Destroy(GameObject.Find(obj));
        }
    
    }


    public void WhichSong(string song)
    {
        finalSongPath = null;
        if(songList != null )
        
        {
            MusicToPlay = song + ".wav";
            finalSongPath = musicPath+MusicToPlay;
            Debug.Log(finalSongPath);
            StartCoroutine(PlayMusic());
            


        }

        
        

    }


    



}
