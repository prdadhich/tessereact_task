using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LoadJson : MonoBehaviour
{
    // Start is called before the first frame update

    private string jsonPath = "D:/TessMusicPlayer/TessMusicPlayer.json";
    public MusicData musicData;

    void Start()
    {
        string jsonString = File.ReadAllText(jsonPath);
        musicData = JsonUtility.FromJson<MusicData>(jsonString);
       // Debug.Log(musicData.music[0].name[]);  
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [System.Serializable]
    public class MusicData
    {

        public Music[] music;

    }
    [System.Serializable]
    public class Music
    {
        public string playlistname;
        public string[] name;
       
    
    }
}
