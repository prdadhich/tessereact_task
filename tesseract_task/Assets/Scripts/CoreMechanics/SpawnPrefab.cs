using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;

public class SpawnPrefab : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject prefab;

   
  
    
    private int numberToSpawn = 0;
    public float Radius = 5.0f;
    [HideInInspector]
    public int index = 0;
    [HideInInspector]
    public List<GameObject> obj = new List<GameObject>();
    public List<Vector3> transformPos = new List<Vector3>(); 





    // Update is called once per frame


    public void Spawn(int number)
    {
        if (number < 6)
        {
            numberToSpawn = number;
        }
        else
        {
            numberToSpawn = 6;
        }


        float  angleToRotate = (360 / numberToSpawn)*Mathf.Deg2Rad;
        Vector3 spawnLocation = new Vector3(0, Radius, 0);
        float x1 = spawnLocation.x;
        float y1 = spawnLocation.y;

        for (int i = 0;i < numberToSpawn; i++)
        {
           
           if(i != 0)
            {
                float x2 = (x1 * Mathf.Cos(angleToRotate)) - (y1 * Mathf.Sin(angleToRotate));
                float y2 = (x1 * Mathf.Sin(angleToRotate)) + (y1 * Mathf.Cos(angleToRotate));
                spawnLocation.x = x2;
                spawnLocation.y = y2;
                spawnLocation.z = 0;
                x1 = x2;
                y1 = y2;
                index = i;
               
            }
          // setName.Name(index);

            var Obj = Instantiate(prefab, spawnLocation, Quaternion.identity) as GameObject;
            transformPos.Add(spawnLocation);
            obj.Add(Obj);
            

           
        }

    }


    public void LoadPlaylist()
    {

        Debug.Log("working ");
    }

    
     public void ResetPos(string name)
    {
        int index = -1;
        for(int i=0; i<obj.Count;i++)
        {
            if(obj[i].name == name)
            {
                index= i;
            }
        }
     

        GameObject.Find(name).transform.position = transformPos[index];
        GameObject.Find(name).gameObject.GetComponent<GetHoverObject>().ResetRotate();


    }



}
